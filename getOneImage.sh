#!/bin/bash
FILENAME=capture.jpg
SKIPFRAMES=15
CAMERA_SUBSTRING=Webcam

# delete existing file
echo deleting $FILENAME
rm $FILENAME

# capture image (1st try)
fswebcam -d /dev/video0 --no-banner -r 960x720 --jpeg 85 -S $SKIPFRAMES $FILENAME

#reset usb device if no image was captured
if [ ! -f "$FILENAME" ]; then

	# get usb bus and device
	LSUSB_GREPPED=$(lsusb | grep "$CAMERA_SUBSTRING")

	# restart camera if found
	if [ -n "$LSUSB_GREPPED" ]; then
		IFS=': ' read -a ARRAY <<< "$LSUSB_GREPPED"
		./usbreset /dev/bus/usb/"${ARRAY[1]}"/"${ARRAY[3]}"
	fi

	#wait until device exists
	while [ ! -c /dev/video0 ]; do
		echo "waiting for device..."
		sleep 1
	done

	# capture image (2nd try)
	fswebcam -d /dev/video0 --no-banner -r 960x720 --jpeg 85 -S $SKIPFRAMES $FILENAME
fi



