// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var exec       = require('sync-exec');
var fs         = require('fs');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8888;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'Hooray! Welcome to the birdwatch api!' });
});

function captureImageAsDataURL() {
    // Capture an image
    console.log('Capturing an image...');
    exec('./getOneImage.sh');
    console.log('Done.');

    // Get file content
    var data = fs.readFileSync('capture.jpg');
    console.log('File content read.');

    var dataURL = "data:image/jpeg;base64," + data.toString('base64');
    return dataURL;
}

function getCapture(req, res) {
    var result = { };
    result.imageData = captureImageAsDataURL();
    res.json(result);
}

router.route('/capture').get(getCapture);

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/birdwatch-api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Birds and apples are served on port ' + port);
